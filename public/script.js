let step = 5;
let full = 1000;
let stepsCount = full / step;
let run = false;

let stepFunction = (element) => {
  let cprcnt = element.data("cprcnt");
  let prcnt = element.data("prcn");

  if (cprcnt < prcnt) {
    cprcnt += element.data("perStep");
    element.data("cprcnt", cprcnt);

    setTimeout(() => stepFunction(element), step);
  }

  element.css("width", element.data("cprcnt") + "%");
};

function scroll_style() {
  let windowTop = $(window).scrollTop();
  let windowHeight = $(window).height();
  let skillsElement = $("#skills");
  let divTop = skillsElement.offset().top;
  let divHeight = skillsElement.height();

  if (
    !run &&
    windowTop + windowHeight > divTop &&
    windowTop < divTop + divHeight
  ) {
    run = true;
    $(".progress-bar").each((_i, e) => {
      $(e).css("transition", "0");
      let element = $(e);

      let prcnt = element.data("prcn");
      element.data("cprcnt", 0);
      element.data("perStep", prcnt / stepsCount);

      stepFunction(element);
    });
  }
}

$(function () {
  $(window).scroll(scroll_style);
  scroll_style();
});

let mybutton = document.getElementById("btn-back-to-top");

window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

mybutton.addEventListener("click", backToTop);

function backToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
